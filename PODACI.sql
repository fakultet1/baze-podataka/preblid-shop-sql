CREATE OR REPLACE PACKAGE podaci AS
    PROCEDURE p_spremi_korisnika (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    );

    PROCEDURE p_spremi_kategoriju (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    );

    PROCEDURE p_spremi_proizvod (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    );

    PROCEDURE p_spremi_zaposlenika (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    );

END podaci;
/


CREATE OR REPLACE PACKAGE BODY podaci AS

    e_iznimka EXCEPTION;
---------------------------------KORISNIK-----------------------------------------------------------
    PROCEDURE p_spremi_korisnika (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) AS

        l_obj       json_object_t;
        l_string    VARCHAR2(1000);
        l_korisnici korisnici%rowtype;
        l_message   VARCHAR2(300);
        l_errcod    NUMBER(3);
        l_output    VARCHAR2(3000);
        l_id        NUMBER;
        l_ime       VARCHAR2(50);
        l_prezime   VARCHAR2(50);
        l_password  VARCHAR2(60);
        l_ulica     VARCHAR2(60);
        l_kucbr     NUMBER;
        l_dodkucbr  VARCHAR2(20);
        l_grad      VARCHAR2(60);
        l_pozmob    NUMBER;
        l_brmob     NUMBER;
        l_radmj     VARCHAR2(20);
        l_email     VARCHAR2(40);
        l_datrod    DATE;
        l_datzap    DATE;
        l_prrad     DATE;
        l_action    VARCHAR2(20);
    BEGIN
        l_obj := json_object_t(in_json);
        l_string := in_json.to_string;
        IF ( filter.f_check_korisnici(l_obj, out_json) or  biznis.b_check_korisnici(l_obj, out_json) ) THEN
            RAISE e_iznimka;
        END IF;
        l_output := l_obj.to_string;
        common.p_logiraj('whatever', json_value(l_output, '$.h_errcod'));
        BEGIN
            SELECT
                JSON_VALUE(l_string, '$.ID'),
                JSON_VALUE(l_string, '$.IME'),
                JSON_VALUE(l_string, '$.PREZIME'),
                JSON_VALUE(l_string, '$.EMAIL'),
                JSON_VALUE(l_string, '$.PASSWORD'),
                JSON_VALUE(l_string, '$.ULICA'),
                JSON_VALUE(l_string, '$.KUCBR'),
                JSON_VALUE(l_string, '$.DODKUCBR'),
                JSON_VALUE(l_string, '$.GRAD'),
                JSON_VALUE(l_string, '$.POZMOB'),
                JSON_VALUE(l_string, '$.BRMOB'),
                JSON_VALUE(l_string, '$.AKCIJA')
            INTO
                l_korisnici.id,
                l_korisnici.ime,
                l_korisnici.prezime,
                l_korisnici.email,
                l_korisnici.password,
                l_korisnici.ulica,
                l_korisnici.kucbr,
                l_korisnici.dodkucbr,
                l_korisnici.grad,
                l_korisnici.pozmob,
                l_korisnici.brmob,
                l_action
            FROM
                dual;

        EXCEPTION
            WHEN OTHERS THEN
                RAISE;
        END;

        BEGIN
            IF nvl(l_korisnici.id, 0) = 0 THEN
                INSERT INTO korisnici (
                    ime,
                    prezime,
                    email,
                    password,
                    ulica,
                    kucbr,
                    dodkucbr,
                    grad,
                    pozmob,
                    brmob
                ) VALUES (
                    l_korisnici.ime,
                    l_korisnici.prezime,
                    l_korisnici.email,
                    l_korisnici.password,
                    l_korisnici.ulica,
                    l_korisnici.kucbr,
                    l_korisnici.dodkucbr,
                    l_korisnici.grad,
                    l_korisnici.pozmob,
                    l_korisnici.brmob
                );

                l_message := 'Uspje�no ste unijeli korisnika!';
                l_errcod := 100;
            ELSIF (
                nvl(l_korisnici.id, 0) != 0
                AND l_action = 'DELETE'
            ) THEN
                DELETE FROM korisnici
                WHERE
                    id = l_korisnici.id;

                l_message := 'Uspje�no ste obrisali korisnika!';
                l_errcod := 100;
            ELSE
                UPDATE korisnici
                SET
                    email = l_korisnici.email
                WHERE
                    id = l_korisnici.id;

                l_message := 'Uspje�no ste izmijenili korisnika!';
                l_errcod := 100;
            END IF;
        EXCEPTION
            WHEN OTHERS THEN
                RAISE;
        END;

        l_obj.put('h_message', l_message);
        l_obj.put('h_errcod', l_errcod);
        out_json := l_obj;
        COMMIT;
    EXCEPTION
        WHEN e_iznimka THEN
            ROLLBACK;
            out_json := l_obj;
        WHEN OTHERS THEN
            ROLLBACK;
            l_message := 'Do�lo je do pogre�ke u sustavu. Molimo poku�ajte ponovno!';
            l_errcod := 999;
            common.p_errlog('p_spremi_predmet', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
            l_obj.put('h_message', l_message);
            l_obj.put('h_errcod', l_errcod);
            out_json := l_obj;
    END p_spremi_korisnika;
---------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------KATEGORIJE-------------------------------------------
    PROCEDURE p_spremi_kategoriju (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) AS

        l_obj        json_object_t;
        l_string     VARCHAR2(1000);
        l_kategorije kategorije%rowtype;
        l_message    VARCHAR2(300);
        l_errcod     NUMBER(3);
        l_output     VARCHAR2(3000);
        l_id         NUMBER;
        l_naziv      VARCHAR2(60);
        l_action     VARCHAR2(60);
    BEGIN
        l_obj := json_object_t(in_json);
        l_string := in_json.to_string;
        IF ( filter.f_check_kategorije(l_obj, out_json) or biznis.b_check_kategorije(l_obj, out_json) ) THEN
            RAISE e_iznimka;
        END IF;
        l_output := l_obj.to_string;
        common.p_logiraj('whatever', json_value(l_output, '$.h_errcod'));
        BEGIN
            SELECT
                JSON_VALUE(l_string, '$.ID'),
                JSON_VALUE(l_string, '$.NAZIV'),
                JSON_VALUE(l_string, '$.AKCIJA')
            INTO
                l_kategorije.id,
                l_kategorije.naziv,
                l_action
            FROM
                dual;

        EXCEPTION
            WHEN OTHERS THEN
                RAISE;
        END;

        BEGIN
            IF nvl(l_kategorije.id, 0) = 0 THEN
                INSERT INTO kategorije (
                    id,
                    naziv
                ) VALUES (
                    l_kategorije.id,
                    l_kategorije.naziv
                );

                l_message := 'Uspje�no ste unijeli kategorije!';
                l_errcod := 100;
            ELSIF (
                nvl(l_kategorije.id, 0) != 0
                AND l_action = 'DELETE'
            ) THEN
                DELETE FROM kategorije
                WHERE
                    id = l_kategorije.id;

                l_message := 'Uspje�no ste obrisali kategoriju!';
                l_errcod := 100;
            ELSE
                UPDATE kategorije
                SET
                    id = l_kategorije.id
                WHERE
                    id = l_kategorije.id;

                l_message := 'Uspje�no ste izmijenili kategorije!';
                l_errcod := 100;
            END IF;
        EXCEPTION
            WHEN OTHERS THEN
                RAISE;
        END;

        l_obj.put('h_message', l_message);
        l_obj.put('h_errcod', l_errcod);
        out_json := l_obj;
        COMMIT;
    EXCEPTION
        WHEN e_iznimka THEN
            ROLLBACK;
            out_json := l_obj;
        WHEN OTHERS THEN
            ROLLBACK;
            l_message := 'Do�lo je do pogre�ke u sustavu. Molimo poku�ajte ponovno!';
            l_errcod := 999;
            common.p_errlog('p_spremi_kategorije', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
            l_obj.put('h_message', l_message);
            l_obj.put('h_errcod', l_errcod);
            out_json := l_obj;
    END p_spremi_kategoriju;
----------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------Proizvod------------------------------------------------

    PROCEDURE p_spremi_proizvod (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) AS

        l_obj       json_object_t;
        l_string    VARCHAR2(1000);
        l_proizvod  proizvod%rowtype;
        l_message   VARCHAR2(300);
        l_errcod    NUMBER(3);
        l_id        NUMBER;
        l_idvrproiz NUMBER;
        l_naziv     VARCHAR2(60);
        l_brand     VARCHAR2(60);
        l_opis      VARCHAR2(500);
        l_cijena    NUMBER(8, 2);
        l_output    VARCHAR2(3000);
        l_action    VARCHAR2(300);
    BEGIN
        l_obj := json_object_t(in_json);
        l_string := in_json.to_string;
        IF ( filter.f_check_proizvod(l_obj, out_json) or  biznis.b_check_proizvod(l_obj, out_json) ) THEN
            RAISE e_iznimka;
        END IF;
        l_output := l_obj.to_string;
        common.p_logiraj('whatever', json_value(l_output, '$.h_errcod'));
        BEGIN
            SELECT
                JSON_VALUE(l_string, '$.ID'),
                JSON_VALUE(l_string, '$.IDVRPROIZ'),
                JSON_VALUE(l_string, '$.BRAND'),
                JSON_VALUE(l_string, '$.OPIS'),
                JSON_VALUE(l_string, '$.NAZIV'),
                JSON_VALUE(l_string, '$.CIJENA'),
                JSON_VALUE(l_string, '$.AKCIJA')
            INTO
                l_proizvod.id,
                l_proizvod.idvrproiz,
                l_proizvod.brand,
                l_proizvod.opis,
                l_proizvod.naziv,
                l_proizvod.cijena,
                l_action
            FROM
                dual;

        EXCEPTION
            WHEN OTHERS THEN
                RAISE;
        END;

        BEGIN
            IF nvl(l_proizvod.id, 0) = 0 THEN
                INSERT INTO proizvod (
                    id,
                    idvrproiz,
                    naziv,
                    brand,
                    opis,
                    cijena
                ) VALUES (
                    l_proizvod.id,
                    l_proizvod.idvrproiz,
                    l_proizvod.naziv,
                    l_proizvod.brand,
                    l_proizvod.opis,
                    l_proizvod.cijena
                );

                l_message := 'Uspje�no ste unijeli proizvod!';
                l_errcod := 100;
            ELSIF (
                nvl(l_proizvod.id, 0) != 0
                AND l_action = 'DELETE'
            ) THEN
                DELETE FROM proizvod
                WHERE
                    id = l_proizvod.id;

                l_message := 'Uspje�no ste obrisali proizvod!';
                l_errcod := 100;
            ELSE
                UPDATE proizvod
                SET
                    id = l_proizvod.id,
                    idvrproiz = l_proizvod.idvrproiz,
                    naziv = l_proizvod.naziv,
                    brand = l_proizvod.brand,
                    opis = l_proizvod.opis,
                    cijena = l_proizvod.cijena
                WHERE
                    id = l_proizvod.id;

                l_message := 'Uspje�no ste izmijenili proizvod!';
                l_errcod := 100;
            END IF;
        EXCEPTION
            WHEN OTHERS THEN
                RAISE;
        END;

        l_obj.put('h_message', l_message);
        l_obj.put('h_errcod', l_errcod);
        out_json := l_obj;
        COMMIT;
    EXCEPTION
        WHEN e_iznimka THEN
            ROLLBACK;
            out_json := l_obj;
        WHEN OTHERS THEN
            ROLLBACK;
            l_message := 'Do�lo je do pogre�ke u sustavu. Molimo poku�ajte ponovno!';
            l_errcod := 999;
            common.p_errlog('p_spremi_proizvod', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
            l_obj.put('h_message', l_message);
            l_obj.put('h_errcod', l_errcod);
            out_json := l_obj;
    END p_spremi_proizvod;
  
 ------------------------------------------------------------------------------------------------------------------------------ 
  
  
  ------------------------------------------------------------Zaposlenici-------------------------------------------------------
    PROCEDURE p_spremi_zaposlenika (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) AS

        l_obj          json_object_t;
        l_string       VARCHAR2(1000);
        l_zaposlenici  zaposlenici%rowtype;
        l_message      VARCHAR2(300);
        l_errcod       NUMBER(3);
        l_id           NUMBER;
        l_ime          VARCHAR2(300);
        l_prezime      VARCHAR2(300);
        l_oib          NUMBER(11, 0);
        l_pozmob       NUMBER(3, 0);
        l_brmob        NUMBER(9, 0);
        l_radmj        VARCHAR2(20);
        l_email        VARCHAR2(40);
        l_datrod       DATE;
        l_idposlovnice NUMBER;
        l_datzap       DATE;
        l_output       VARCHAR2(3000);
        l_action       VARCHAR2(3000);
    BEGIN
        l_obj := json_object_t(in_json);
        l_string := in_json.to_string;
        IF ( filter.f_check_zaposlenici(json_object_t(l_string), l_obj) or biznis.b_check_zaposlenici(json_object_t(l_string), l_obj) ) THEN
            RAISE e_iznimka;
        END IF;

        l_output := l_obj.to_string;
        common.p_logiraj('whatever', json_value(l_output, '$.h_errcod'));
        BEGIN
            SELECT
                JSON_VALUE(l_string, '$.ID'),
                JSON_VALUE(l_string, '$.IME'),
                JSON_VALUE(l_string, '$.PREZIME'),
                JSON_VALUE(l_string, '$.OIB'),
                JSON_VALUE(l_string, '$.POZMOB'),
                JSON_VALUE(l_string, '$.BRMOB'),
                JSON_VALUE(l_string, '$.RADMJ'),
                JSON_VALUE(l_string, '$.EMAIL'),
                to_date(JSON_VALUE(l_string, '$.DATROD'), 'DD.MM.RR'),
                to_date(JSON_VALUE(l_string, '$.DATZAP'), 'DD.MM.RR'),
                JSON_VALUE(l_string, '$.IDPOSLOVNICE'),
                JSON_VALUE(l_string, '$.AKCIJA')
            INTO
                l_zaposlenici.id,
                l_zaposlenici.ime,
                l_zaposlenici.prezime,
                l_zaposlenici.oib,
                l_zaposlenici.pozmob,
                l_zaposlenici.brmob,
                l_zaposlenici.radmj,
                l_zaposlenici.email,
                l_zaposlenici.datrod,
                l_zaposlenici.datzap,
                l_zaposlenici.idposlovnice,
                l_action
            FROM
                dual;

        EXCEPTION
            WHEN OTHERS THEN
                RAISE;
        END;

        BEGIN
            IF nvl(l_zaposlenici.id, 0) = 0 THEN
                INSERT INTO zaposlenici (
                    ime,
                    prezime,
                    oib,
                    pozmob,
                    brmob,
                    radmj,
                    email,
                    datrod,
                    datzap,
                    idposlovnice
                ) VALUES (
                    l_zaposlenici.ime,
                    l_zaposlenici.prezime,
                    l_zaposlenici.oib,
                    l_zaposlenici.pozmob,
                    l_zaposlenici.brmob,
                    l_zaposlenici.radmj,
                    l_zaposlenici.email,
                    l_zaposlenici.datrod,
                    l_zaposlenici.datzap,
                    l_zaposlenici.idposlovnice
                );

                l_message := 'Uspje�no ste unijeli zaposlenika!';
                l_errcod := 100;
            ELSIF (
                nvl(l_zaposlenici.id, 0) != 0
                AND l_action = 'DELETE'
            ) THEN
                DELETE FROM zaposlenici
                WHERE
                    id = l_zaposlenici.id;

                l_message := 'Uspje�no ste obrisali zaposlenika!';
                l_errcod := 100;
            ELSE
                UPDATE zaposlenici
                SET
                    email = l_zaposlenici.email,
                    ime = l_zaposlenici.ime,
                    prezime = l_zaposlenici.prezime,
                    oib = l_zaposlenici.oib,
                    pozmob = l_zaposlenici.pozmob,
                    brmob = l_zaposlenici.brmob,
                    radmj = l_zaposlenici.radmj,
                    datrod = l_zaposlenici.datrod,
                    datzap = l_zaposlenici.datzap,
                    idposlovnice = l_zaposlenici.idposlovnice
                WHERE
                    id = l_zaposlenici.id;

                l_message := 'Uspje�no ste izmijenili zaposlenika!';
                l_errcod := 100;
            END IF;
        EXCEPTION
            WHEN OTHERS THEN
                RAISE;
        END;

        l_obj.put('h_message', l_message);
        l_obj.put('h_errcod', l_errcod);
        out_json := l_obj;
        COMMIT;
    EXCEPTION
        WHEN e_iznimka THEN
            ROLLBACK;
            out_json := l_obj;
        WHEN OTHERS THEN
            ROLLBACK;
            l_message := 'Do�lo je do pogre�ke u sustavu. Molimo poku�ajte ponovno!';
            l_errcod := 999;
            common.p_errlog('p_spremi_zaposlenika', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
            --l_obj.put('h_message', l_message);
            --l_obj.put('h_errcod', l_errcod);
            out_json := l_obj;
    END p_spremi_zaposlenika;

END podaci;
/
