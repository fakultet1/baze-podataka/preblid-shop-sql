

DROP TABLE stavke;

DROP TABLE racuni;

DROP TABLE stanje;

DROP TABLE proizvod;

DROP TABLE vrsta;

DROP TABLE kategorije;

DROP TABLE zaposlenici;

DROP TABLE poslovnice;

DROP TABLE jamstvo;

DROP TABLE korisnici;


DROP SEQUENCE korisnici_id_seq;

DROP SEQUENCE stanje_id_seq;

DROP SEQUENCE poslovnice_id_seq;

DROP SEQUENCE stavke_id_seq;

DROP SEQUENCE proizvod_id_seq;

DROP SEQUENCE jamstvo_id_seq;

DROP SEQUENCE vrsta_id_seq;

DROP SEQUENCE kategorije_id_seq;

DROP SEQUENCE racuni_id_seq;

DROP SEQUENCE zaposlenici_id_seq;






CREATE TABLE Zaposlenici (
	ID NUMBER(9, 0) NOT NULL,
	ime VARCHAR2(50) NOT NULL,
	prezime VARCHAR2(50) NOT NULL,
	OIB NUMBER(11, 0) NOT NULL,
	pozMob NUMBER(3, 0) NOT NULL,
	brMob NUMBER(9, 0) NOT NULL,
	radMj VARCHAR2(20) NOT NULL,
	email VARCHAR2(40) NOT NULL,
	datRod DATE NOT NULL,
	datZap DATE NOT NULL,
	prRad DATE NULL,
	CreBy VARCHAR2(40)default user NOT NULL,
	UpdBy VARCHAR2(40) NOT NULL,
	CreDate TIMESTAMP NOT NULL,
	LastUpd TIMESTAMP,
	IDposlovnice NUMBER(9, 0) NOT NULL,
	 CONSTRAINT zaposlenici_pk PRIMARY KEY ( id ),
    CONSTRAINT constraint_radmj CHECK (UPPER(radmj) in ('PRODAVAC','DIREKTOR','VODITELJ_POSLOVNICE')),
    CONSTRAINT constraint_pozmob CHECK (pozmob in (98,91,92,97,95,99)),
    CONSTRAINT constraint_email CHECK (email like('%__@__%.__%'))
	);
--- Tablica Zaposlenici sadrzi informacije o zaposlenicima kao sto su ID,ime,preszime,OIB,mobitel,radno mjesto,e mail,datum rodenja,datum zaposlenja,datum prestanka rada,
-----te informacije o osobi koja je kreirala tablicu,updatala tablicu,datum kreiranja tablice,te posljednji update

--sekvenca za tablicu Zaposlenici
CREATE sequence ZAPOSLENICI_ID_SEQ;

CREATE OR REPLACE TRIGGER bi_zaposlenici_id BEFORE
    INSERT ON zaposlenici
    FOR EACH ROW
BEGIN
    SELECT
        zaposlenici_id_seq.NEXTVAL
    INTO :new.id
    FROM
        dual;
	SELECT
        user
    INTO :new.updby
    FROM
        dual;
END;
/

CREATE OR REPLACE TRIGGER bi_zaposlenici_credate BEFORE
    INSERT ON zaposlenici
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO 
	   :new.credate
    FROM
        dual;

    SELECT
        user
    INTO :new.updby
    FROM
        dual;

END;
/

--kreiranje triggeera kod update-a
CREATE OR REPLACE TRIGGER bi_zaposlenici_lastupd BEFORE
    UPDATE ON zaposlenici
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.lastupd
    FROM
        dual;

    SELECT
        user
    INTO :new.updby
    FROM
        dual;

END;
/

--Kreiraje tablice Vrsta Proizvoda
CREATE TABLE Vrsta (
	ID NUMBER(9, 0) NOT NULL,
	naziv VARCHAR2(40) NOT NULL,
	IDkatproiz NUMBER(9, 0) NOT NULL,
	CreBy VARCHAR2(40)default user NOT NULL,
	Updby VARCHAR2(40) NOT NULL,
	CreDate TIMESTAMP NOT NULL,
	LastUpd TIMESTAMP,
	constraint VRSTA_PK PRIMARY KEY (ID));
-- Tablica Vrsta proizvoda sadrzi podatke o proizvodima kao sto su: ID,naziv,koja je kategorija proizvoda. Sadrzi takoder informacije o osobi koja je kreirala tablicu,datum kreiranja,posljedni update

--Kreiranje sekvence za tablicu vrsta proizvoda
CREATE sequence VRSTA_ID_SEQ;

CREATE trigger BI_VRSTA_ID
  before insert on Vrsta
  for each row
begin
  select VRSTA_ID_SEQ.nextval into :NEW.ID from dual;
end;

/


--kreiranje triggera kod credate i lastupd

CREATE OR REPLACE TRIGGER bi_vrsta_credate BEFORE
    INSERT ON vrsta
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.credate
    FROM
        dual;


END;
/
--kreiranje triggeera kod update-a
CREATE OR REPLACE TRIGGER bi_vrsta_lastupd BEFORE
    UPDATE ON zaposlenici
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.lastupd
    FROM
        dual;

    SELECT
        user
    INTO :new.updby
    FROM
        dual;

END;
/
--Kreiranje tablice stanje u Poslovnicama
CREATE TABLE Stanje (
	ID NUMBER(9, 0) NOT NULL,
	kolicina NUMBER(20, 0) NOT NULL,
	IDposl NUMBER(9, 0) NOT NULL,
	IDproiz NUMBER(9, 0) NOT NULL,
	CreBy VARCHAR2(40)default user NOT NULL,
	Updby VARCHAR2(40) NOT NULL,
	CreDate TIMESTAMP NOT NULL,
	LastUpd TIMESTAMP,
	constraint STANJE_PK PRIMARY KEY (ID));
-- Tablica stanje u poslovnicama sadrzi podatke o ID-u,kolicina proizvoda u poslovnicama,ID poslovnice(veza na tablicu poslovnice),ID proizvoda(veza na tablicu proizvodi),
-- sadrzi informacije o kreatoru tablice,datumu kreiranja,posljednjem update-u.

--sekvenca za tablicu stanje u poslovnicama
CREATE sequence STANJE_ID_SEQ;

CREATE trigger BI_STANJE_ID
  before insert on Stanje
  for each row
begin
  select STANJE_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE OR REPLACE TRIGGER bi_stanje_credate BEFORE
    INSERT ON stanje
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.credate
    FROM
        dual;

	SELECT
        user
    INTO :new.updby
    FROM
        dual;
END;
/
CREATE OR REPLACE TRIGGER bi_stanje_lastupd BEFORE
    UPDATE ON stanje
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.lastupd
    FROM
        dual;

    SELECT
        user
    INTO :new.updby
    FROM
        dual;

END;
/

--Kreiranje tablice Racuni
CREATE TABLE Racuni (
	ID NUMBER(9, 0) NOT NULL,
	IDzapos NUMBER(9, 0) NOT NULL,
	porez DECIMAL(4,2) NOT NULL,
	ukupno DECIMAL(38,2) NOT NULL,
	datrac DATE NOT NULL,
	datval DATE NOT NULL,
	IDposl NUMBER(9, 0) NOT NULL,
	CreBy VARCHAR2(40)default user NOT NULL,
	UpdBy VARCHAR2(40) NOT NULL,
	CreDate TIMESTAMP NOT NULL,
	LastUpd TIMESTAMP,
	IDkorisnika NUMBER(9, 0),
	constraint RACUNI_PK PRIMARY KEY (ID),
	CONSTRAINT constraint_porez CHECK (porez in(25,15,10,5,13)));
-- tablica Racuni sadrzi podatke : ID,ID zaposlenika,porez(stopa),ukupno(kune),datum izdanog racuna,datum placenog racuna,ID poslovnice(u kojem je napravljen i izdan racun),tko je kreirao tablicu,
    --tko ju je update-ao,datum kreiranja,i poslijednji update nad tablicom
    
--Kreiranje sekvence za tablicu racuni
CREATE sequence RACUNI_ID_SEQ;

CREATE trigger BI_RACUNI_ID
  before insert on Racuni
  for each row
begin
  select RACUNI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/

CREATE OR REPLACE TRIGGER bi_racuni_credate BEFORE
    INSERT ON racuni
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.credate
    FROM
        dual;
	SELECT
        user
    INTO :new.updby
    FROM
        dual;

END;
/
--kreiranje triggeera 
CREATE OR REPLACE TRIGGER bi_racuni_lastupd BEFORE
    UPDATE ON racuni
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.lastupd
    FROM
        dual;

    SELECT
        user
    INTO :new.updby
    FROM
        dual;

END;
/

--Kreiranje tablice Poslovnice 
CREATE TABLE Poslovnice (
	ID NUMBER(9, 0) NOT NULL,
	naziv VARCHAR2(40) NOT NULL,
	ulica VARCHAR2(20) NOT NULL,
	kucBr NUMBER(5, 0) NOT NULL,
	DodKucBr VARCHAR2(10) NULL,
	drzava VARCHAR2(20) NOT NULL,
	grad VARCHAR2(20) NOT NULL,
	CreBy VARCHAR2(40)default user NOT NULL,
	UpdBy VARCHAR2(40) NOT NULL,
	CreDate TIMESTAMP NOT NULL,
	LastUpd TIMESTAMP,
	constraint POSLOVNICE_PK PRIMARY KEY (ID));
--Tablica poslovnice sadrzi informacije: ID,naziv poslovnice,ulica,kucni broj,drzava,grad,ID zaposlenika(veza na tablicu zaposlenici,da znam u kojoj poslovnici radi koji zaposlenik uveden u bazu)
    --tko je kreirao tablicu,tko ju je update-ao,datum kreiranja,te poslijednji update
    
--Kreiranje sekvence za tablicu Poslovnice
CREATE sequence POSLOVNICE_ID_SEQ;

CREATE trigger BI_POSLOVNICE_ID
  before insert on Poslovnice
  for each row
begin
  select POSLOVNICE_ID_SEQ.nextval into :NEW.ID from dual;
end;

/

CREATE OR REPLACE TRIGGER bi_poslovnice_credate BEFORE
    INSERT ON poslovnice
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.credate
    FROM
        dual;

	SELECT
        user
    INTO :new.updby
    FROM
        dual;
END;
/
--kreiranje triggeera 
CREATE OR REPLACE TRIGGER bi_poslovnice_lastupd BEFORE
    UPDATE ON poslovnice
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.lastupd
    FROM
        dual;

    SELECT
        user
    INTO :new.updby
    FROM
        dual;

END;
/

--Kreiranje tablice Proizvod
CREATE TABLE Proizvod (
	ID NUMBER(9, 0) NOT NULL,
	IDvrproiz NUMBER(9, 0) NOT NULL,
	brand VARCHAR2(70) NOT NULL,
	opis VARCHAR2(500) NOT NULL,
	naziv VARCHAR2(60) NOT NULL,
	cijena DECIMAL(10,2) NOT NULL,
	Creby VARCHAR2(40)default user NOT NULL,
	UpdBy VARCHAR2(40) NOT NULL,
	CreDate TIMESTAMP NOT NULL,
	LastUpd TIMESTAMP,
	constraint PROIZVOD_PK PRIMARY KEY (ID));

CREATE sequence PROIZVOD_ID_SEQ;

CREATE trigger BI_PROIZVOD_ID
  before insert on Proizvod
  for each row
begin
  select PROIZVOD_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE OR REPLACE TRIGGER bi_proizvod_credate BEFORE
    INSERT ON proizvod
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.credate
    FROM
        dual;

	SELECT
        user
    INTO :new.updby
    FROM
        dual;
END;
/
--kreiranje triggeera 
CREATE OR REPLACE TRIGGER bi_proizvod_lastupd BEFORE
    UPDATE ON proizvod
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.lastupd
    FROM
        dual;

    SELECT
        user
    INTO :new.updby
    FROM
        dual;

END;
/

--Kreiranje tablice kategorije proizvoda
CREATE TABLE Kategorije (
	ID NUMBER(9, 0) NOT NULL,
	naziv VARCHAR2(50) NOT NULL,
	CreBy VARCHAR2(40)default user NOT NULL,
	UpdBy VARCHAR2(40) NOT NULL,
	CreDate TIMESTAMP NOT NULL,
	LastUpd TIMESTAMP,
	constraint KATEGORIJE_PK PRIMARY KEY (ID));

CREATE sequence KATEGORIJE_ID_SEQ;

CREATE trigger BI_KATEGORIJE_ID
  before insert on Kategorije
  for each row
begin
  select KATEGORIJE_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE OR REPLACE TRIGGER bi_kategorije_credate BEFORE
    INSERT ON kategorije
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.credate
    FROM
        dual;

	SELECT
        user
    INTO :new.updby
    FROM
        dual;
END;
/
--kreiranje triggeera 
CREATE OR REPLACE TRIGGER bi_kategorije_lastupd BEFORE
    UPDATE ON kategorije
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.lastupd
    FROM
        dual;

    SELECT
        user
    INTO :new.updby
    FROM
        dual;

END;
/
--Kreiranje tablice Racun stavke
CREATE TABLE Stavke (
	ID NUMBER(9, 0) NOT NULL,
	IDracun NUMBER(9, 0) NOT NULL,
	IDproizvod NUMBER(9, 0) NOT NULL,
	kolicina NUMBER(10, 0) NOT NULL,
	cijena DECIMAL(10,2) NOT NULL,
	popust DECIMAL(10,2) NULL,
	iznos DECIMAL(10,2) NOT NULL,
	CreBy VARCHAR2(40)default user NOT NULL,
	UpdBy VARCHAR2(40) NOT NULL,
	CreDate TIMESTAMP NOT NULL,
	LastUpd TIMESTAMP,
	IDjamstvo NUMBER(9, 0),
	constraint STAVKE_PK PRIMARY KEY (ID));

CREATE sequence STAVKE_ID_SEQ;

CREATE trigger BI_STAVKE_ID
  before insert on Stavke
  for each row
begin
  select STAVKE_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE OR REPLACE TRIGGER bi_stavke_credate BEFORE
    INSERT ON stavke
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.credate
    FROM
        dual;

	SELECT
        user
    INTO :new.updby
    FROM
        dual;
END;
/
--kreiranje triggeera 
CREATE OR REPLACE TRIGGER bi_stavke_lastupd BEFORE
    UPDATE ON stavke
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.lastupd
    FROM
        dual;

    SELECT
        user
    INTO :new.updby
    FROM
        dual;

END;
/

--Kreiranje tablice Jamstvo
CREATE TABLE Jamstvo (
	ID NUMBER(9, 0) NOT NULL,
	duljina NUMBER(3, 0) NOT NULL,
	jedtraj VARCHAR2(10) NOT NULL,
	CreBy VARCHAR2(40)default user NOT NULL,
	UpdBy VARCHAR2(40) NOT NULL,
	CreDate TIMESTAMP NOT NULL,
	LastUpd TIMESTAMP,
	constraint JAMSTVO_PK PRIMARY KEY (ID),
    CONSTRAINT constraint_jedtraj CHECK (UPPER(jedtraj) in ('MJESEC','GODINA','DAN'))
    );


--sadrzi podatke:ID,duljina jamstva,jedinica trajanja(dan,mjesec,godina),datum kupnje,ID racuna
    --informacije tko je kreirao tablicu,tko je update-ao tablicu,datum kreiranja,posljednji update
--Kreiranje sekvence za tablicu racun stavke 
CREATE SEQUENCE jamstvo_id_seq;

--Kreiranje triggera za tablicu racun stavke

CREATE TRIGGER bi_jamstvo_id BEFORE
    INSERT ON jamstvo
    FOR EACH ROW
BEGIN
    SELECT
        jamstvo_id_seq.NEXTVAL
    INTO :new.id
    FROM
        dual;
	SELECT
        user
    INTO :new.updby
    FROM
        dual;
END;
/
CREATE OR REPLACE TRIGGER bi_jamstvo_credate BEFORE
    INSERT ON jamstvo
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.credate
    FROM
        dual;


END;
/
--kreiranje triggeera 
CREATE OR REPLACE TRIGGER bi_jamstvo_lastupd BEFORE
    UPDATE ON jamstvo
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.lastupd
    FROM
        dual;

    SELECT
        user
    INTO :new.updby
    FROM
        dual;

END;
/

CREATE TABLE Korisnici (
	id NUMBER(9, 0) NOT NULL,
	ime VARCHAR2(60) NOT NULL,
	prezime VARCHAR2(60) NOT NULL,
	email VARCHAR2(60) NOT NULL,
	password VARCHAR2(60) NOT NULL,
	ulica VARCHAR2(60) NOT NULL,
	kucbr NUMBER(5, 0) NOT NULL,
	dodkucbr VARCHAR2(20) NULL,
	grad VARCHAR2(60) NOT NULL,
	pozmob NUMBER(3, 0) NOT NULL,
	brmob NUMBER(9, 0) NOT NULL,
	creby VARCHAR2(40) default user NOT NULL,
	credate TIMESTAMP NOT NULL,
	lastupd TIMESTAMP,
	updby VARCHAR2(40) NOT NULL,
	constraint KORISNICI_PK PRIMARY KEY (id),
    CONSTRAINT constraint_emailK CHECK (email like('%__@__%.__%')));


--kreiranje sekvence
CREATE sequence KORISNICI_ID_SEQ;


--trigger
CREATE trigger BI_KORISNICI_ID
  before insert on Korisnici
  for each row
begin
  select KORISNICI_ID_SEQ.nextval into :NEW.id from dual;
end;

/
CREATE OR REPLACE TRIGGER bi_korisnici_credate BEFORE
    INSERT ON korisnici
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.credate
    FROM
        dual;
		
	SELECT
        user
    INTO :new.updby
    FROM
        dual;
END;
/
--kreiranje triggeera 
CREATE OR REPLACE TRIGGER bi_korisnici_lastupd BEFORE
    UPDATE ON korisnici
    FOR EACH ROW
BEGIN
    SELECT
        systimestamp
    INTO :new.lastupd
    FROM
        dual;

    SELECT
        user
    INTO :new.updby
    FROM
        dual;

END;
/


ALTER TABLE Zaposlenici ADD CONSTRAINT Zaposlenici_fk0 FOREIGN KEY (IDposlovnice) REFERENCES Poslovnice(ID);

ALTER TABLE Vrsta ADD CONSTRAINT Vrsta_fk0 FOREIGN KEY (IDkatproiz) REFERENCES Kategorije(ID);

ALTER TABLE Stanje ADD CONSTRAINT Stanje_fk0 FOREIGN KEY (IDposl) REFERENCES Poslovnice(ID);
ALTER TABLE Stanje ADD CONSTRAINT Stanje_fk1 FOREIGN KEY (IDproiz) REFERENCES Proizvod(ID);

ALTER TABLE Racuni ADD CONSTRAINT Racuni_fk0 FOREIGN KEY (IDzapos) REFERENCES Zaposlenici(ID);
ALTER TABLE Racuni ADD CONSTRAINT Racuni_fk1 FOREIGN KEY (IDkorisnika) REFERENCES Korisnici(id);


ALTER TABLE Proizvod ADD CONSTRAINT Proizvod_fk0 FOREIGN KEY (IDvrproiz) REFERENCES Vrsta(ID);


ALTER TABLE Stavke ADD CONSTRAINT Stavke_fk0 FOREIGN KEY (IDracun) REFERENCES Racuni(ID);
ALTER TABLE Stavke ADD CONSTRAINT Stavke_fk1 FOREIGN KEY (IDproizvod) REFERENCES Proizvod(ID);
ALTER TABLE Stavke ADD CONSTRAINT Stavke_fk2 FOREIGN KEY (IDjamstvo) REFERENCES Jamstvo(ID);

commit;