CREATE OR REPLACE PACKAGE filter AS
    
    FUNCTION f_check_zaposlenici (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) RETURN BOOLEAN;

    FUNCTION f_check_proizvod (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) RETURN BOOLEAN;

    FUNCTION f_check_kategorije (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) RETURN BOOLEAN;

    FUNCTION f_check_korisnici (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) RETURN BOOLEAN;

END filter;
/


CREATE OR REPLACE PACKAGE BODY filter AS

    e_iznimka EXCEPTION;
--------------------------------------------------------CHECK ZAPOSLENICI---------------------------------------------------
    FUNCTION f_check_zaposlenici (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) RETURN BOOLEAN AS
        l_obj         json_object_t;
        l_zaposlenici zaposlenici%rowtype;
        l_string      VARCHAR2(1000);
        l_page        NUMBER;
        l_perpage     NUMBER;
    BEGIN
        l_obj := json_object_t(in_json);
        l_string := in_json.to_string;
         
        IF ( nvl(JSON_VALUE(l_string, '$.IME'), ' ') = ' ' ) THEN
            l_obj.put('h_message', 'Molimo unesite ime zaposlenika');
            l_obj.put('h_errcode', 101);
            RAISE e_iznimka;
        END IF;

        IF ( nvl(JSON_VALUE(l_string, '$.PREZIME'), ' ') = ' ' ) THEN
            l_obj.put('h_message', 'Molimo unesite prezime zaposlenika');
            l_obj.put('h_errcode', 102);
            RAISE e_iznimka;
        END IF;

        IF ( nvl(JSON_VALUE(l_string, '$.OIB'), 0) = 0 ) THEN
            l_obj.put('h_message', 'Molimo unesite OIB zaposlenika');
            l_obj.put('h_errcode', 103);
            RAISE e_iznimka;
         ELSE
            IF ( LENGTH(JSON_VALUE(l_string, '$.OIB')) !=11 ) THEN
                l_obj.put('h_message', 'OIB je netocan. OIB treba sadrzavati 11 znamenki.');
                l_obj.put('h_errcode', 104);
                RAISE e_iznimka;
            END IF;
        END IF;

        IF ( nvl(JSON_VALUE(l_string, '$.POZMOB'), 0) = 0 ) THEN
            l_obj.put('h_message', 'Molimo unesite pozivni broj mobitela zaposlenika');
            l_obj.put('h_errcode', 105);
            RAISE e_iznimka;
        ELSE
            IF ( JSON_VALUE(l_string, '$.POZMOB') NOT IN ( 98, 97, 91, 92, 95,
                                               99 ) ) THEN
                l_obj.put('h_message', 'Pozivni broj je netocan. Pozivni broj moze biti: 98,99,97,92,91,95.');
                l_obj.put('h_errcode', 106);
                RAISE e_iznimka;
            END IF;
        END IF;
  
        IF ( nvl(JSON_VALUE(l_string, '$.BRMOB'), 0) = 0) THEN
            l_obj.put('h_message', 'Molimo unesite broj mobitela zaposlenika');
            l_obj.put('h_errcode', 107);
            RAISE e_iznimka;
        ELSE
            IF ( LENGTH(JSON_VALUE(l_string, '$.BRMOB')) > 9 ) THEN
                l_obj.put('h_message', 'Broj mobitela mora sadrzavati najvise 9 znamenki bez pozivnog broja');
                l_obj.put('h_errcode', 133);
            RAISE e_iznimka;
            END IF;
        END IF;
       
        IF ( JSON_VALUE(l_string, '$.RADMJ') NOT IN ('PRODAVAC','DIREKTOR','VODITELJ_POSLOVNICE') ) THEN
            l_obj.put('h_message', 'Radno mjesto moze biti: PRODAVAC, DIREKTOR, ili VODITELJ_POSLOVNICE. Molimo upisite jedno od navedenih radnih mjesta');
            l_obj.put('h_errcode', 108);
            RAISE e_iznimka;
        END IF;
        IF ( nvl(JSON_VALUE(l_string, '$.EMAIL'), ' ') = ' ' ) THEN
            l_obj.put('h_message', 'Molimo unesite email zaposlenika');
            l_obj.put('h_errcode', 109);
            RAISE e_iznimka;
        END IF;

        IF ( nvl(to_date(JSON_VALUE(l_string, '$.DATROD'),'DD.MM.YYYY'), to_date('01.01.1500','DD.MM.YYYY')) = to_date('01.01.1500','DD.MM.YYYY') ) THEN
            l_obj.put('h_message', 'Molimo unesite datum rodenja zaposlenika');
            l_obj.put('h_errcode', 110);
            RAISE e_iznimka;
        END IF;

        IF ( nvl(to_date(JSON_VALUE(l_string, '$.DATZAP'),'DD.MM.YYYY'), to_date('01.01.1500','DD.MM.YYYY')) = to_date('01.01.1500','DD.MM.YYYY') ) THEN
            l_obj.put('h_message', 'Molimo unesite datum zaposlenja zaposlenika');
            l_obj.put('h_errcode', 111);
            RAISE e_iznimka;
        END IF;

        IF ( nvl(JSON_VALUE(l_string, '$.IDPOSLOVNICE'), 0) = 0 ) THEN
            l_obj.put('h_message', 'Molimo unesite id poslovnice zaposlenika');
            l_obj.put('h_errcode', 112);
            RAISE e_iznimka;
        END IF; 
        
        out_json := l_obj;
        RETURN false;
    EXCEPTION
        WHEN e_iznimka THEN
        out_json := l_obj;
            RETURN true;
        WHEN OTHERS THEN
            common.p_errlog('p_check_zaposlenici', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
            l_obj.put('h_message', 'Dogodila se gre�ka u obradi podataka!');
            l_obj.put('h_errcode', 113);
            out_json := l_obj;
            RETURN true;
    END f_check_zaposlenici;
    
    --------------------------PROIZVOD-----------------------------------------------------------------
    FUNCTION f_check_proizvod (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) RETURN BOOLEAN AS
        l_obj         json_object_t;
        l_proizvod proizvod%rowtype;
        l_string      VARCHAR2(1000);
        l_page        NUMBER;
        l_perpage     NUMBER;
    BEGIN
        l_obj := json_object_t(in_json);
        l_string := in_json.to_string;
        SELECT
            JSON_VALUE(l_string, '$.ID'),
            JSON_VALUE(l_string, '$.IDVRPROIZ'),
            JSON_VALUE(l_string, '$.BRAND'),
            JSON_VALUE(l_string, '$.OPIS'),
            JSON_VALUE(l_string, '$.NAZIV'),
            JSON_VALUE(l_string, '$.CIJENA')   
        INTO
            l_proizvod.id,
            l_proizvod.idvrproiz,
            l_proizvod.brand,
            l_proizvod.opis,
            l_proizvod.naziv,
            l_proizvod.cijena
        
        FROM
            dual;

        IF ( nvl(l_proizvod.brand, ' ') = ' ' ) THEN
            l_obj.put('h_message', 'Molimo unesite brand proizvoda');
            l_obj.put('h_errcode', 114);
            RAISE e_iznimka;
        END IF;

        IF ( nvl(l_proizvod.opis, ' ') = ' ' ) THEN
            l_obj.put('h_message', 'Molimo unesite opis proizvoda');
            l_obj.put('h_errcode', 115);
            RAISE e_iznimka;
        END IF;

        IF ( nvl(l_proizvod.naziv, ' ') = ' ' ) THEN
            l_obj.put('h_message', 'Molimo unesite naziv proizvoda');   --ovdje puca zato jer usporeduje string i number,ne moze string biti =0 
            l_obj.put('h_errcode', 116);
            RAISE e_iznimka;
        END IF;


        IF ( nvl(l_proizvod.cijena, 0) = 0 ) THEN
            l_obj.put('h_message', 'Molimo unesite cijenu proizvoda');
            l_obj.put('h_errcode', 117);
            RAISE e_iznimka;
        END IF;


        out_json := l_obj;
        RETURN false;
    EXCEPTION
        WHEN e_iznimka THEN
        out_json := l_obj;
            RETURN true;
        WHEN OTHERS THEN
            common.p_errlog('p_check_proizvod', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
            l_obj.put('h_message', 'Dogodila se gre�ka u obradi podataka!');
            l_obj.put('h_errcode', 118);
            out_json := l_obj;
            RETURN true;
    END f_check_proizvod;
    ------------------------------------------------KATEGORIJE---------------------------------------------------------------
    FUNCTION f_check_kategorije (
        in_json  IN json_object_t,
        out_json OUT json_object_t) RETURN BOOLEAN AS
        l_obj         json_object_t;
        l_kategorije kategorije%rowtype;
        l_string      VARCHAR2(1000);
        l_page        NUMBER;
        l_perpage     NUMBER;
    BEGIN
        l_obj := json_object_t(in_json);
        l_string := in_json.to_string;
        SELECT
            JSON_VALUE(l_string, '$.ID'),
            JSON_VALUE(l_string, '$.NAZIV')
        INTO
            l_kategorije.id,
            l_kategorije.naziv
           
        FROM
            dual;

        IF ( nvl(l_kategorije.naziv, ' ') = ' ' ) THEN
            l_obj.put('h_message', 'Molimo unesite naziv kategorije');
            l_obj.put('h_errcode', 119);
            RAISE e_iznimka;
        END IF;

        out_json := l_obj;
        RETURN false;
    EXCEPTION
        WHEN e_iznimka THEN
        out_json := l_obj;
            RETURN true;
        WHEN OTHERS THEN
            common.p_errlog('p_check_kategorije', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
            l_obj.put('h_message', 'Dogodila se gre�ka u obradi podataka!');
            l_obj.put('h_errcode', 120);
            out_json := l_obj;
            RETURN true;
    END f_check_kategorije;
    
    ---------------------------------------------KORISNICI------------------------------------------------------
    FUNCTION f_check_korisnici (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) RETURN BOOLEAN AS
        l_obj         json_object_t;
        l_korisnici korisnici%rowtype;
        l_string      VARCHAR2(1000);
        l_page        NUMBER;
        l_perpage     NUMBER;
    BEGIN
        l_obj := json_object_t(in_json);
        l_string := in_json.to_string;
       
     
        IF ( nvl( JSON_VALUE(l_string, '$.IME'), ' ') = ' ' ) THEN
            l_obj.put('h_message', 'Molimo unesite ime korisnika');
            l_obj.put('h_errcode', 121);
            RAISE e_iznimka;
        END IF;

        IF ( nvl( JSON_VALUE(l_string, '$.PREZIME'), ' ') = ' ' ) THEN
            l_obj.put('h_message', 'Molimo unesite prezime korisnika');
            l_obj.put('h_errcode', 122);
            RAISE e_iznimka;
        END IF;

        IF ( nvl( JSON_VALUE(l_string, '$.EMAIL'), ' ') = ' ' ) THEN
            l_obj.put('h_message', 'Molimo unesite email korisnika');
            l_obj.put('h_errcode', 123);
            RAISE e_iznimka;
        END IF;
        
         IF ( nvl(JSON_VALUE(l_string, '$.PASSWORD'), ' ') = ' ' ) THEN
            l_obj.put('h_message', 'Molimo unesite lozinku');
            l_obj.put('h_errcode', 124);
            RAISE e_iznimka;
        END IF;
        
          IF ( nvl(JSON_VALUE(l_string, '$.ULICA'),' ') = ' ' ) THEN
            l_obj.put('h_message', 'Molimo unesite ulicu');
            l_obj.put('h_errcode', 125);
            RAISE e_iznimka;
        END IF;
        
         IF ( nvl(JSON_VALUE(l_string, '$.KUCBR'), 0) = 0 ) THEN
            l_obj.put('h_message', 'Molimo unesite kucni broj');
            l_obj.put('h_errcode', 126);
            RAISE e_iznimka;
        END IF;
        
          IF ( nvl(JSON_VALUE(l_string, '$.GRAD'), ' ') = ' ' ) THEN
            l_obj.put('h_message', 'Molimo unesite grad');
            l_obj.put('h_errcode', 127);
            RAISE e_iznimka;
        END IF;

        IF ( nvl(JSON_VALUE(l_string, '$.POZMOB'), 0) = 0 ) THEN
            l_obj.put('h_message', 'Molimo unesite pozivni broj mobitela zaposlenika');
            l_obj.put('h_errcode', 128);
            RAISE e_iznimka;
        ELSE
            IF ( l_korisnici.pozmob NOT IN ( 98, 97, 91, 92, 95,
                                               99 ) ) THEN
                l_obj.put('h_message', 'Pozivni broj je netocan');
                l_obj.put('h_errcode', 129);
                RAISE e_iznimka;
            END IF;
        END IF;

        IF ( nvl(LENGTH(JSON_VALUE(l_string, '$.BRMOB')), 0) = 0 ) THEN
            l_obj.put('h_message', 'Molimo unesite broj mobitela zaposlenika');
            l_obj.put('h_errcode', 130);
            RAISE e_iznimka;
        ELSE
            IF (LENGTH( JSON_VALUE(l_string, '$.BRMOB')) > 10 ) THEN
                l_obj.put('h_message', 'Broj mobitela mora sadrzavati najvise 9 znamenki bez pozivnog broja');
                l_obj.put('h_errcode', 131);
            RAISE e_iznimka;
            END IF;
        END IF;



        out_json := l_obj;
        RETURN false;
    EXCEPTION
        WHEN e_iznimka THEN
        out_json := l_obj;
            RETURN true;
        WHEN OTHERS THEN
            common.p_errlog('p_check_korisnici', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
            l_obj.put('h_message', 'Dogodila se gre�ka u obradi podataka!');
            l_obj.put('h_errcode', 132);
            out_json := l_obj;
            RETURN true;
    END f_check_korisnici;
    
    
    
END filter;
/
