--1.dohvat svih proizvoda uz opis,vrstu proizvoda,ID te proizvodaca
SELECT
    proizvod.id,
    vrsta.naziv AS "Vrsta Proizvoda",
    proizvod.brand,
    proizvod.opis
FROM
    proizvod,
    vrsta
WHERE
    proizvod.idvrproiz = vrsta.id
ORDER BY
    proizvod.id;


--2.dohvat svih proizvoda sa nazivom proizvodaca,nazivom proizvoda,opisom i cijenom, bez jamstva
SELECT
    proizvod.id,
    proizvod.brand      AS proizvodac,
    proizvod.naziv,
    proizvod.opis,
    proizvod.cijena
FROM
    proizvod;
    
--3.dohvat naziv proizvoda sa nazivom kategorije
SELECT
    vrsta.id,
    vrsta.naziv,
    kategorije.naziv AS "NAZIV KATEGORIJE"
FROM
    vrsta,
    kategorije
WHERE
    kategorije.id = vrsta.IDKATPROIZ;


-- 4.dohvat svih korisnika,spojene su kolone pozivnog broja mobitela i broj mobitela te je dodana nula ispored pozivnog broja, bez spola  
SELECT
    ime,
    prezime,
    email,
    concat(lpad(pozmob, 3, '0'), ' ' || brmob) AS "BROJ MOBITELA"
FROM
    korisnici;

--nemoguce, potrebna zamjena (dohvat svih racuna, IDZAPOS,POREZ,UKUPNO,DATRAC,DATVAL,IDPOSL i npr izracunaj vrijednost poreza -> ukupno * porez)
--5.dohvat tablice dobavljaci,tablica dobavljaci je povezana sa tablicom poslovnice. kolona id_posl je zamjenjena nazivom poslovnice
SELECT
    idzapos,
    porez,
    ukupno,
   (porez/100) *ukupno as "IZNOS POREZA",
    datrac,
    datval,
    idposl
FROM
    racuni;
    
--6.stanje u poslovnicama,dohvaceno je ime poslovnice,naziv proizvoda,vrsta proizvoda,kolicina proizvoda ovisno o poslovnici
SELECT
    stanje.kolicina,
    poslovnice.naziv AS poslovnica,
    proizvod.naziv   AS proizvod,
    vrsta.naziv
FROM
    stanje,
    poslovnice,
    proizvod,
    vrsta
WHERE
        stanje.idposl = poslovnice.id
    AND stanje.idproiz = proizvod.id;

--7.Dohvacam sve proizvode koji se nalaze u poslovnici 2

SELECT
    kolicina,
    poslovnice.naziv,
    proizvod.naziv       AS proizvod,
    vrsta.naziv AS "VRSTA PROIZVODA"
FROM
    stanje,
    poslovnice,
    proizvod,
    vrsta
WHERE
        stanje.idposl = poslovnice.id
    AND poslovnice.id = 2;
    
 --8.dohvacam sve racune iz poslovnice 1 sa datumom izdavanja racuna,datumom placanja,zaposlenik koji je izvrsio naplatu,porez
SELECT
    concat(zaposlenici.ime, ' ' || zaposlenici.prezime) AS zaposlenik,
    concat(racuni.porez, '' || '%')                     AS porez,
    to_char(ukupno, '999999D00')                        AS ukupno,
    datrac                                             AS "DATUM IZDAVANJA RACUNA",
    datval                                            AS "DATUM PLACANJA",
    poslovnice.naziv                                    AS poslovnica
FROM
    racuni,
    poslovnice,
    zaposlenici
WHERE
        racuni.idzapos = zaposlenici.id
    AND poslovnice.id = racuni.idposl
    AND poslovnice.id = 1;


--9. zelim sve informacije o voditeljima poslovnice
SELECT
    ime,
    prezime,
    oib,
    pozmob,
    brmob,
    radmj  AS "RADNO MJESTO",
    email,
    datrod AS "DATUM RODENJA",
    datzap AS "DATUM ZAPOSLENJA",
    prrad  AS "PRESTANAK RADA"
FROM
    zaposlenici
WHERE
     zaposlenici.radmj like 'VODITELJ_POSLOVNICE';
     
--10.dohvaceni proioizvodi za racun ciji je id=2
SELECT
    *
FROM
    stavke,
    racuni
WHERE
   stavke.idracun=racuni.id and racuni.id=2;
    