--UPDATE

--pomoc kod update-a
select * FROM proizvod;


--1. update-am zaposlenika Sandro Markovic i mijenjam njegov broj mobitela
update zaposlenici

set pozmob=91,
    brmob=6985004  
where id=1;

--2.Sofija se udala pa joj je promijenjeno prezime

update zaposlenici

set ime='Sofija',
    prezime='Babi� Markovi�',
    email='sbmarkovic@prebild.hr',
    updby='NKRIVANEK',
    lastupd=sysdate
where id=4;

--3.Bojana Jukic je dobila/dala otkaz pa joj je u kolonu prestanak rada dodan danasnji datum,i promjenjen joj je mail jer njen sluzbebi mail vise nije aktivan

update zaposlenici

set prrad=sysdate,
    updby='NKRIVANEK',
    lastupd=sysdate,
    email='bjukic@gmail.com'
where id=8;

--4.promijenjen je mail Tonki Pejic

update zaposlenici

set email='tpecij@prebild.hr',
    updby='NKRIVANEK',
    lastupd=sysdate
where id=6;

--5.promijenjena je cijena na proizvodu HUAWEI p40 lite,cijena sada iznosi 2500 kuna dok je prije iznosila 1666,55 kuna


--pomoc kod update-a
select 
    proizvod.id,
    vrsta.naziv as "VRSTA PROIZVODA",
    proizvod.naziv,
   proizvod.brand,
    proizvod.cijena,
   proizvod.opis,
   proizvod.updby,
   proizvod.lastupd,
   jamstvo.duljina || jamstvo.jed_traj as JAMSTVO
  
from proizvod,
    vrsta,
    jamstvo
where proizvod.id_vrsta_proiz=vrsta.id and proizvod.id_jamstvo=jamstvo.id;

update proizvod
set cijena='2500,00',
    updby='NKRIVAEK',
    lastupd=sysdate
where id=19;

--6.promijenjena je cijena MICROSOFT office 2019 iz 2049 kuna u 1464 kune
    
update proizvod
  
set cijena=1464,
    updby='NKRIVANEK',
    lastupd=sysdate
where id=13;   

--7.promijenjena je cijena na proizvodu logitech shifter driving force iz 461,11 kuna u 424,15 kuna

update proizvod 

set cijena='424,15',
    updby='NKRIVANEK',
    lastupd=sysdate
where id=26;

--8.mijenjam jamstvo za proizvod logitech drving force iz 24 mjeseca u 12 mjeseci ( mijenjam jamstvo preko id-a)

update proizvod 

set opis='volan sa 6 brzina',
    updby='NKRIVANEK',
    lastupd=sysdate
where id=26;

--9.mijenjam cijenu i jamstvo za proizvod USB stick asus,mijenjam i ime jer je prvotno krivo napisano (prvo je u koloni proiz pisalo AUSUS sada pise ASUS

update proizvod

set brand='ASUS',
    cijena='835,00',
    updby='NKRIVANEK',
    lastupd=sysdate
where id=22;

--10.promijenjen je opis proizvoda Cannon tonera C-exv 14 cija je cijena 461,41 kunu

update proizvod 

set opis='Broj stranica: 8300
            Boja: Crna',
    updby='NKRIVANEK',
    lastupd=sysdate
where id=25;

--11.updateam stanje u poslovnicama,zbog toga sam sa selectom si napravila pomoc da vidim podatke kouje mijenjam. Mijenjam kolicinu mobitela apple 12 mini u poslovnici Prebild 2



update stanje
set kolicina=1,
    updby='NKRIVANEK',
    lastupd=sysdate
where id=38;




--12.mijenjam kolicinu proizvoda Nitendo swich koji se nalazi u poslovnici prebild 1,mijenjam kolicinu iz 44 u 35

update stanje
set kolicina=35,
    updby='NKRIVANEK',
    lastupd=sysdate
where id=65;

--13.mijenjam kolicinu proizvoda Epson  its l3160 u poslovnici Prebild 2 prije update-a kolicina proizvoda je bila 2,sada ima 14 takvih proizvoda u navedenoj poslovnici


update stanje
set kolicina=14,
    updby='NKRIVANEK',
    lastupd=sysdate
where id=18;

--14.mijenjam kolicinu proizvoda marke KINGSTON memorija pc-21300 u poslovnici 1. id=1,kolicina je bila 2,sada je 10

update stanje
set kolicina=10,
    updby='NKRIVANEK',
    lastupd=sysdate
where id=1;

--15.Mia Dujmovic je na porodiljnom pa joj je promijenjen status radnog mjesta

select * from zaposlenici;

update zaposlenici 
set radmj='PRODAVAC',
    updby='NKRIVANEK',
    lastupd=sysdate
where id=14;

--16.Niksa Horvat je promijenio broj mobitela
update zaposlenici--ne radi
set pozmob='98',
    brmob='7651233'
    
where id=5;

--17.mijenjam opis i cijenu proizvoda Razer basilisk V2 misa,cijena je bila 525 kuna,a sada je 456,84 kuna

update proizvod
set opis='Raser mis V2 hand USB type-A optical 20000 DPI',
    updby='NKRIVANEK',
    lastupd=sysdate,
    cijena='456,84'
where id=2;   

--18.mijenjam naziv kategorije proizvoda(iz gaming i zabava u igre i zabava) u tablici kategorije proizvoda

update kategorije
set naziv='Igre i zabava',
    updby='NKRIVANEK',
    lastupd=sysdate
where id=11;

--19.Vlatka Klobucar je postala zamjenica voditelja
select * from zaposlenici;

update zaposlenici
set radmj='VODITELJ_POSLOVNICE',
    updby='NKRIVANEK',
    lastupd=sysdate
where id=7;

--20.mijenjam cjenu proizvodu razer book 13,prijasnja cijena je 13499 kuna,nova cijena je 11639,03 kune

update proizvod
set cijena='11639,03',
    updby='NKRIVANEK',
    lastupd=sysdate
where id=5;

--DELETE

--svi podaci koji su obrisani su se nalazili u tablici vrsta proizvoda. Navedeni nazivi vrsta proizvoda koji su obrisani u tablici vise nisu potrebni jer se vise ne prodaju u Prebild prodavaonicama 

--1.izbrisan je red u tablci vrsta proizvoda. Red je sadrzavao vrstu proizvoda: podloga za mis (vrsta proizvoda nije dostupna u niti jednoj prodavaonici). Red je izbrisan preko id-a
delete from vrsta
where id=8;

--2.Izbrisan je red u tablici vrsta proizvoda. Red je sadrzavao vrstu proizvoda: Monitor. Delete je izvrsen preko naziva  u tablici vrsta proizvoda

delete from vrsta
where  naziv='Monitor';

--3.Brisem red u tablici vrsta proizvoda. Red je sadrzavao vrstu proizvoda:Matica ploca. Delete je izvrsen preko id-a
delete from vrsta
where id=11;

--4.Obrisan je red u tablici vrsta proizvoda. Red je sadrzavao vrstu proizvoda: Laptop za internet i office. dDelete je izvrsen preko naziva

delete from vrsta
where naziv='Laptop za Internet i office';

--5.Obrisan je red u tablici vrsta proizvoda. Red je sadrzavao vrstu proizvoda: Wi-fi kamere. Delete je izvrsen preko id-a

delete from vrsta
where id=24;

--6.Obrisan je red u tablici vrsta proizvoda. Red je sadrzavao vrstu proizvoda:Audio kabel. Delete je izvrsen preko id-a

delete from vrsta
where id=38;

--7. Obrisan je red u tablici vrsta proizvoda. Red je sadrzavao vrstu proizvoda: punjac. Delete je izvrsen preko naziva vrste proizvoda
delete from vrsta
where naziv='Punja�';

--8.Obrisan je red u tablici vrsta proizvoda. Red je sadrzavao vrstu proizvoda:Baterija. Delete je izvrsen preko id-a

delete from vrsta
where id=57;

--9.Obrisan je red u tablici vrsta proizvoda. Red je sadrzavao vrstu proizvoda:Igre> Delete je izvrsen preko naziva vrste proizvoda 

delete from vrsta
where naziv='Igre';
