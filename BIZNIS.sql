CREATE OR REPLACE PACKAGE biznis AS
    e_iznimka EXCEPTION;
    FUNCTION b_check_kategorije (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) RETURN BOOLEAN;

    FUNCTION b_check_proizvod (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) RETURN BOOLEAN;

    FUNCTION b_check_zaposlenici (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) RETURN BOOLEAN;

    FUNCTION b_check_korisnici (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) RETURN BOOLEAN;

END biznis;
/


CREATE OR REPLACE PACKAGE BODY biznis AS
-------------------------------------------------b_check_kategorije-------------------------------------------------------------------------
    FUNCTION b_check_kategorije (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) RETURN BOOLEAN AS

        l_obj        json_object_t;
        l_kategorije kategorije%rowtype;
        l_string     VARCHAR2(1000);
        l_page       NUMBER;
        l_perpage    NUMBER;
        l_count      NUMBER;
        l_action     VARCHAR2(20);
    BEGIN
        l_obj := json_object_t(in_json);
        l_string := in_json.to_string;
        SELECT
            JSON_VALUE(l_string, '$.ID'),
            JSON_VALUE(l_string, '$.NAZIV'),
            JSON_VALUE(l_string, '$.AKCIJA')
        INTO
            l_kategorije.id,
            l_kategorije.naziv,
            l_action
        FROM
            dual;

        SELECT
            COUNT(id)
        INTO l_count
        FROM
            kategorije
        WHERE
            naziv = l_kategorije.naziv;

        IF (l_count != 0 AND nvl(l_action, ' ') != 'DELETE') THEN
            l_obj.put('h_message', 'Naziv kategorije vec postoji');
            l_obj.put('h_errcode', 119);
            RAISE e_iznimka;
        END IF;

        out_json := l_obj;
        RETURN false;
    EXCEPTION
        WHEN e_iznimka THEN
            out_json := l_obj;
            RETURN true;
        WHEN OTHERS THEN
            common.p_errlog('b_check_kategorije', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
            l_obj.put('h_message', 'Dogodila se gre�ka u obradi podataka!');
            l_obj.put('h_errcode', 120);
            out_json := l_obj;
            RETURN true;
    END b_check_kategorije;
---------------------------------------------------------------b_check_proizvod---------------------------------------------------------------------------------------------
    FUNCTION b_check_proizvod (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) RETURN BOOLEAN AS

        l_obj       json_object_t;
        l_proizvod  proizvod%rowtype;
        l_string    VARCHAR2(1000);
        l_page      NUMBER;
        l_perpage   NUMBER;
        l_count     NUMBER;
        l_id        NUMBER;
        l_brand     VARCHAR2(3000);
        l_naziv     VARCHAR2(500);
        l_idvrproiz NUMBER;
        l_action    VARCHAR2(20);
    BEGIN
        l_obj := json_object_t(in_json);
        l_string := in_json.to_string;
        SELECT
            JSON_VALUE(l_string, '$.ID'),
            JSON_VALUE(l_string, '$.BRAND'),
            JSON_VALUE(l_string, '$.NAZIV'),
            JSON_VALUE(l_string, '$.IDVRPROIZ'),
            JSON_VALUE(l_string, '$.AKCIJA')
        INTO
            l_proizvod.id,
            l_proizvod.brand,
            l_proizvod.naziv,
            l_proizvod.idvrproiz,
            l_action
        FROM
            dual;

        SELECT
            COUNT(id)
        INTO l_count
        FROM
            proizvod
        WHERE
                naziv = l_proizvod.naziv
            AND brand = l_proizvod.brand
            AND idvrproiz = l_proizvod.idvrproiz;

        IF (l_count != 0 AND nvl(l_action, ' ') != 'DELETE') THEN
            l_obj.put('h_message', 'Proizvod vec postoji');
            l_obj.put('h_errcode', 121);
            RAISE e_iznimka;
        END IF;

        out_json := l_obj;
        RETURN false;
    EXCEPTION
        WHEN e_iznimka THEN
            out_json := l_obj;
            RETURN true;
        WHEN OTHERS THEN
            common.p_errlog('b_check_proizvod', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
            l_obj.put('h_message', 'Dogodila se gre�ka u obradi podataka!');
            l_obj.put('h_errcode', 122);
            out_json := l_obj;
            RETURN true;
    END b_check_proizvod;
    
    
----------------------------------------------------------------b_check_zaposlenici------------------------------------------------------------------------------------
    FUNCTION b_check_zaposlenici (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) RETURN BOOLEAN AS

        l_obj         json_object_t;
        l_zaposlenici zaposlenici%rowtype;
        l_string      VARCHAR2(1000);
        l_page        NUMBER;
        l_perpage     NUMBER;
        l_count       NUMBER;
        l_oib         NUMBER;
        l_action      VARCHAR2(20);
    BEGIN
        l_obj := json_object_t(in_json);
        l_string := in_json.to_string;
        SELECT
            JSON_VALUE(l_string, '$.ID'),
            JSON_VALUE(l_string, '$.OIB'),
            JSON_VALUE(l_string, '$.AKCIJA')
        INTO
            l_zaposlenici.id,
            l_zaposlenici.oib,
            l_action
        FROM
            dual;

        SELECT
            COUNT(id)
        INTO l_count
        FROM
            zaposlenici
        WHERE
            oib = l_zaposlenici.oib; ---countam id spremam ga u l_count iz tablice zaposlenici gdje je oib iz tablice jednak unesenom oibu preko postmana

        IF (l_count != 0 AND nvl(l_action, ' ') != 'DELETE') THEN
            l_obj.put('h_message', 'Zaposlenik vec postoji');
            l_obj.put('h_errcode', 123);
            RAISE e_iznimka;
        END IF;

        out_json := l_obj;
        RETURN false;
    EXCEPTION
        WHEN e_iznimka THEN
            out_json := l_obj;
            RETURN true;
        WHEN OTHERS THEN
            common.p_errlog('b_check_zaposlenici', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
            l_obj.put('h_message', 'Dogodila se gre�ka u obradi podataka!');
            l_obj.put('h_errcode', 124);
            out_json := l_obj;
            RETURN true;
    END b_check_zaposlenici;
    
------------------------------------------------------------------b_check_korisnici---------------------------------------------------------------------------------------------------
    FUNCTION b_check_korisnici (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) RETURN BOOLEAN AS

        l_obj       json_object_t;
        l_korisnici korisnici%rowtype;
        l_string    VARCHAR2(1000);
        l_page      NUMBER;
        l_perpage   NUMBER;
        l_count     NUMBER;
        l_ime       VARCHAR2(60);
        l_prezime   VARCHAR2(60);
        l_email     VARCHAR2(60);
        l_pozmob    NUMBER;
        l_brmob     NUMBER;
        l_action    VARCHAR2(20);
    BEGIN
        l_obj := json_object_t(in_json);
        l_string := in_json.to_string;
        SELECT
            JSON_VALUE(l_string, '$.ID'),
            JSON_VALUE(l_string, '$.IME'),
            JSON_VALUE(l_string, '$.PREZIME'),
            JSON_VALUE(l_string, '$.EMAIL'),
            JSON_VALUE(l_string, '$.POZMOB'),
            JSON_VALUE(l_string, '$.BRMOB'),
            JSON_VALUE(l_string, '$.AKCIJA')
        INTO
            l_korisnici.id,
            l_korisnici.ime,
            l_korisnici.prezime,
            l_korisnici.email,
            l_korisnici.pozmob,
            l_korisnici.brmob,
            l_action
        FROM
            dual;

        SELECT
            COUNT(id)
        INTO l_count
        FROM
            korisnici
        WHERE
                ime = l_korisnici.ime
            AND prezime = l_korisnici.prezime
            AND email = l_korisnici.email
            AND pozmob = l_korisnici.pozmob
            AND brmob = l_korisnici.brmob;

        IF (l_count != 0 AND nvl(l_action, ' ') != 'DELETE') THEN
            l_obj.put('h_message', 'Korisnik vec postoji');
            l_obj.put('h_errcode', 125);
            RAISE e_iznimka;
        END IF;

        out_json := l_obj;
        RETURN false;
    EXCEPTION
        WHEN e_iznimka THEN
            out_json := l_obj;
            RETURN true;
        WHEN OTHERS THEN
            common.p_errlog('b_check_korisnici', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
            l_obj.put('h_message', 'Dogodila se gre�ka u obradi podataka!');
            l_obj.put('h_errcode', 126);
            out_json := l_obj;
            RETURN true;
    END b_check_korisnici;

END biznis;
/
