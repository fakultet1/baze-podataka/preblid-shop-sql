--11.zelim sve zaposlenike ispod 30 godina
SELECT
    *
FROM
    zaposlenici
WHERE
    datrod BETWEEN '1.1.1991' AND sysdate;
    
--12.zelim sve zaposlenike koji su zaposleni 2020.godine
SELECT
    datzap,
    zaposlenici.*
FROM
    zaposlenici
WHERE
        datzap > TO_DATE('1.1.2020', 'DD.MM.YYYY')
    AND datzap < TO_DATE('1.1.2021', 'DD.MM.YYYY');
   
--13.poredala sam zaposlenike po broju prodanih proizvoda
SELECT
    concat(zaposlenici.ime, ' ' || zaposlenici.prezime) AS naziv,
    SUM(stavke.kolicina)
FROM
    racuni,
    stavke,
    zaposlenici
WHERE
        zaposlenici.id = racuni.idzapos
    AND stavke.idracun = racuni.id
GROUP BY
    concat(zaposlenici.ime, ' ' || zaposlenici.prezime)
ORDER BY
    SUM(kolicina) DESC;

--14.trazim sve proizvode koji spadaju u kategoriju kablovi (tablica kategorije proizvoda)
SELECT
    vrsta.id,
    vrsta.naziv,
    proizvod.brand,
    proizvod.naziv,
    proizvod.opis,
    proizvod.cijena
FROM
    kategorije,
    proizvod,
    vrsta
WHERE
        kategorije.id = vrsta.idkatproiz
    AND vrsta.id = proizvod.idvrproiz
    AND kategorije.id = 7;
    
--15.svi proizvodi u tablici proizvodi koji  koji pocinju sa slovom C
SELECT
    proizvod.brand AS naziv,
    proizvod.opis
FROM
    proizvod
WHERE
    brand LIKE 'C%';

--16. dohvacam sve proizvode s jednog racuna. ID_RACUNA=9
SELECT
    porez,
    proizvod.cijena,
    proizvod.naziv,
    proizvod.opis,
    proizvod.brand,
    stavke.IDJAMSTVO
FROM
    racuni,
    stavke,
    proizvod
WHERE
        racuni.id = 9
    AND stavke.idracun = racuni.id
    AND stavke.IDPROIZVOD = proizvod.id;

--17.svi proizvodi ispod 50 kuna
SELECT
    *
FROM
    proizvod
WHERE
    cijena < 50;


--18.dohvat svih proizvoda sa vrstom proizvoda od Logitech proizvodaca
SELECT
    proizvod.brand       AS proizvod,
    vrsta.naziv AS "VRSTA PROIZVODA",
    proizvod.naziv,
    proizvod.opis,
    proizvod.cijena
FROM
    proizvod,
    vrsta
WHERE
       
     proizvod.idvrproiz = vrsta.id and proizvod.brand='Logitech';

   
--19.proizvodi cija je cijena visa od 1250 kuna
SELECT
    vrsta.naziv AS "VRSTA PROIZVODA",
    proizvod.brand       AS proizvod,
    proizvod.naziv,
    proizvod.cijena,
    proizvod.opis
FROM
    proizvod,
    vrsta
WHERE
        proizvod.cijena > 1250
    AND proizvod.IDVRPROIZ = vrsta.id;

--20. sortirani proizvodi po cijeni,od najnize do najvise
SELECT
    cijena,
    brand,
    opis,
    naziv
FROM
    proizvod
order by cijena desc;