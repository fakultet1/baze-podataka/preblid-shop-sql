CREATE OR REPLACE PACKAGE DOHVAT AS 

  procedure p_get_zaposlenici(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_get_kategorije(in_json in json_object_t, out_json out json_object_t);
  procedure p_get_korisnici(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_get_proizvod(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_login(in_json in json_object_t, out_json out json_object_t );

END DOHVAT;
/


CREATE OR REPLACE PACKAGE BODY dohvat AS

    e_iznimka EXCEPTION;
------------------------------------------------------------------------------------
--get_zapolenici
    PROCEDURE p_get_zaposlenici (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) AS

        l_obj          json_object_t;
        l_zaposlenici  json_array_t := json_array_t('[]');
        l_count        NUMBER;
        l_id           NUMBER;
        l_ime          VARCHAR2(50);
        l_prezime      VARCHAR2(50);
        l_oib          NUMBER;
        l_pozmob       NUMBER;
        l_brmob        NUMBER;
        l_radmj        VARCHAR2(20);
        l_email        VARCHAR2(40);
        l_datrod       DATE;
        l_datzap       DATE;
        l_prrad        DATE;
        l_idposlovnice NUMBER;
        l_string       VARCHAR2(1000);
        l_search       VARCHAR2(100);
        l_page         NUMBER;
        l_perpage      NUMBER;
    BEGIN
        l_obj := json_object_t(in_json);
        l_string := in_json.to_string;
        SELECT
            JSON_VALUE(l_string, '$.ID'),
            JSON_VALUE(l_string, '$.IME'),
            JSON_VALUE(l_string, '$.PREZIME'),
            JSON_VALUE(l_string, '$.OIB'),
            JSON_VALUE(l_string, '$.POZMOB'),
            JSON_VALUE(l_string, '$.BRMOB'),
            JSON_VALUE(l_string, '$.RADMJ'),
            JSON_VALUE(l_string, '$.EMAIL'),
            JSON_VALUE(l_string, '$.DATROD'),
            JSON_VALUE(l_string, '$.DATZAP'),
            JSON_VALUE(l_string, '$.PRRAD'),
            JSON_VALUE(l_string, '$.IDPOSLOVNICE')
        INTO
            l_id,
            l_ime,
            l_prezime,
            l_oib,
            l_pozmob,
            l_brmob,
            l_radmj,
            l_email,
            l_datrod,
            l_datzap,
            l_prrad,
            l_idposlovnice
        FROM
            dual;

        FOR x IN (
            SELECT
                    JSON_OBJECT(
                        'ID' VALUE id,
                        'IME' VALUE ime,
                        'PREZIME' VALUE prezime,
                        'OIB' VALUE oib,
                        'POZMOB' VALUE pozmob,
                                'BRMOB' VALUE brmob,
                        'RADMJ' VALUE radmj,
                        'EMAIL' VALUE email,
                        'DATROD' VALUE datrod,
                        'DATZAP' VALUE datzap,
                                'PRRAD' VALUE prrad,
                        'IDPOSLOVNICE' VALUE idposlovnice
                    )
                AS izlaz
            FROM
                zaposlenici
            WHERE
                id = nvl(l_id, id)
                --OFFSET NVL(l_page,0) ROWS FETCH NEXT NVL(l_perpage,1) ROWS ONLY
        ) LOOP
            l_zaposlenici.append(json_object_t(x.izlaz));
        END LOOP;

        SELECT
            COUNT(1)
        INTO l_count
        FROM
            zaposlenici;

        l_obj.put('count', l_count);
        l_obj.put('data', l_zaposlenici);
        out_json := l_obj;
    END p_get_zaposlenici; 

--get_korisnici
------------------------------------------------------------------------------------
    PROCEDURE p_get_korisnici (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) AS

        l_obj          json_object_t;
        l_korisnici    json_array_t := json_array_t('[]');
        l_count        NUMBER;
        l_id           NUMBER;
        l_ime          VARCHAR2(50);
        l_prezime      VARCHAR2(50);
        l_password     VARCHAR2(60);
        l_ulica        VARCHAR2(60);
        l_kucbr        NUMBER;
        l_dodkucbr     VARCHAR2(20);
        l_grad         VARCHAR2(60);
        l_pozmob       NUMBER;
        l_brmob        NUMBER;
        l_radmj        VARCHAR2(20);
        l_email        VARCHAR2(40);
        l_datrod       DATE;
        l_datzap       DATE;
        l_prrad        DATE;
        l_idposlovnice NUMBER;
        l_string       VARCHAR2(1000);
        l_search       VARCHAR2(100);
        l_page         NUMBER;
        l_perpage      NUMBER;
    BEGIN
        l_obj := json_object_t(in_json);
        l_string := in_json.to_string;
        SELECT
            JSON_VALUE(l_string, '$.ID'),
            JSON_VALUE(l_string, '$.ime'),
            JSON_VALUE(l_string, '$.prezime'),
            JSON_VALUE(l_string, '$.email'),
            JSON_VALUE(l_string, '$.password'),
            JSON_VALUE(l_string, '$.ulica'),
            JSON_VALUE(l_string, '$.kucbr'),
            JSON_VALUE(l_string, '$.dodkucbr'),
            JSON_VALUE(l_string, '$.grad'),
            JSON_VALUE(l_string, '$.pozmob'),
            JSON_VALUE(l_string, '$.brmob')
        INTO
            l_id,
            l_ime,
            l_prezime,
            l_email,
            l_password,
            l_ulica,
            l_kucbr,
            l_dodkucbr,
            l_grad,
            l_pozmob,
            l_brmob
        FROM
            dual;

        FOR x IN (
            SELECT
                    JSON_OBJECT(
                        'ID' VALUE id,
                        'IME' VALUE ime,
                        'PREZIME' VALUE prezime,
                        'EMAIL' VALUE email,
                        'PASSWIORD' VALUE password,
                                'ULICA' VALUE ulica,
                        'KUCBR' VALUE kucbr,
                        'DODKUCBR' VALUE dodkucbr,
                        'GRAD' VALUE grad,
                        'POZMOB' VALUE pozmob,
                                'BRMOB' VALUE brmob
                    )
                AS izlaz
            FROM
                korisnici
            WHERE
                id = nvl(l_id, id)
        ) LOOP
            l_korisnici.append(json_object_t(x.izlaz));
        END LOOP;

        SELECT
            COUNT(1)
        INTO l_count
        FROM
            korisnici
        WHERE
            id = nvl(l_id, id);

        l_obj.put('count', l_count);
        l_obj.put('data', l_korisnici);
        out_json := l_obj;
    EXCEPTION
        WHEN OTHERS THEN
            common.p_errlog('p_get_korisnici', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_string);
            l_obj.put('h_message', 'Gre�ka u obradi podataka');
            l_obj.put('h_errcode', 99);
            ROLLBACK;
    END p_get_korisnici;
------------------------------------------------------------------------------------Login-------------------------------------------------

 ---------------------------------------------------------------------------------------------------
    PROCEDURE p_get_kategorije (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) AS

        l_obj        json_object_t;
        l_kategorije json_array_t := json_array_t('[]');
        l_count      NUMBER;
        l_id         NUMBER;
        l_string     VARCHAR2(1000);
        l_naziv      VARCHAR2(50);
        l_search     VARCHAR2(100);
        l_page       NUMBER;
        l_perpage    NUMBER;
    BEGIN
        l_obj := json_object_t(in_json);
        l_string := in_json.to_string;
        SELECT
            JSON_VALUE(l_string, '$.ID'),
            JSON_VALUE(l_string, '$.naziv')
        INTO
            l_id,
            l_naziv
        FROM
            dual;

        FOR x IN (
            SELECT
                    JSON_OBJECT(
                        'ID' VALUE id,
                        'NAZIV' VALUE naziv
                    )
                AS izlaz
            FROM
                kategorije
            WHERE
                id = nvl(l_id, id)
              
                --OFFSET NVL(l_page,0) ROWS FETCH NEXT NVL(l_perpage,1) ROWS ONLY
        ) LOOP
            l_kategorije.append(json_object_t(x.izlaz));
        END LOOP;

        SELECT
            COUNT(1)
        INTO l_count
        FROM
            kategorije;
            
        l_obj.put('count', l_count);
        l_obj.put('data', l_kategorije);
        out_json := l_obj;
    END p_get_kategorije;
  

-----------------------------------------------------------------------------------------------------
--get_proizvod
    PROCEDURE p_get_proizvod (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) AS

        l_obj       json_object_t;
        l_proizvodi json_array_t := json_array_t('[]');
        l_count     NUMBER;
        l_id        NUMBER;
        l_idvrproiz NUMBER;
        l_brand     VARCHAR2(70);
        l_opis      VARCHAR2(500);
        l_naziv     VARCHAR2(60);
        l_cijena    NUMBER;
        l_string    VARCHAR2(1000);
        l_search    VARCHAR2(100);
        l_page      NUMBER;
        l_perpage   NUMBER;
    BEGIN
        l_obj := json_object_t(in_json);
        l_string := in_json.to_string;
        SELECT
            JSON_VALUE(l_string, '$.ID'),
            JSON_VALUE(l_string, '$.IDVRPROIZ'),
            JSON_VALUE(l_string, '$.BRAND'),
            JSON_VALUE(l_string, '$.OPIS'),
            JSON_VALUE(l_string, '$.NAZIV'),
            JSON_VALUE(l_string, '$.CIJENA')
        INTO
            l_id,
            l_idvrproiz,
            l_brand,
            l_opis,
            l_naziv,
            l_cijena
        FROM
            dual;

        FOR x IN (
            SELECT
                    JSON_OBJECT(
                        'ID' VALUE id,
                        'IDVRPROIZ' VALUE idvrproiz,
                        'BRAND' VALUE brand,
                        'OPIS' VALUE opis,
                        'NAZIV' VALUE naziv,
                                'CIJENA' VALUE cijena
                    )
                AS izlaz
            FROM
                proizvod
            WHERE
                id = nvl(l_id, id)
                --OFFSET NVL(l_page,0) ROWS FETCH NEXT NVL(l_perpage,1) ROWS ONLY
        ) LOOP
            l_proizvodi.append(json_object_t(x.izlaz));
        END LOOP;

        SELECT
            COUNT(1)
        INTO l_count
        FROM
            proizvod;

        l_obj.put('count', l_count);
        l_obj.put('data', l_proizvodi);
        out_json := l_obj;
    END p_get_proizvod;
  
  
  
  --------------------------------------------------Login-------------------------------------------------

--login
    PROCEDURE p_login (
        in_json  IN json_object_t,
        out_json OUT json_object_t
    ) AS

        l_obj      json_object_t := json_object_t();
        l_input    VARCHAR2(4000);
        l_record   VARCHAR2(4000);
        l_username korisnici.email%TYPE;
        l_password korisnici.password%TYPE;
        l_id       korisnici.id%TYPE;
        l_out      json_array_t := json_array_t('[]');
    BEGIN
        l_obj.put('h_message', '');
    --l_obj.put('h_errcode', '');
        l_input := in_json.to_string;
        SELECT
            JSON_VALUE(l_input, '$.username' RETURNING VARCHAR2),
            JSON_VALUE(l_input, '$.password' RETURNING VARCHAR2)
        INTO
            l_username,
            l_password
        FROM
            dual;

        IF ( l_username IS NULL OR l_password IS NULL ) THEN
            l_obj.put('h_message', 'Molimo unesite korisni�ko ime i zaporku');
            l_obj.put('h_errcod', 101);
            RAISE e_iznimka;
        ELSE
            BEGIN
                SELECT
                    id
                INTO l_id
                FROM
                    korisnici
                WHERE
                        email = l_username
                    AND password = l_password;

            EXCEPTION
                WHEN no_data_found THEN
                    l_obj.put('h_message', 'Nepoznato korisni�ko ime ili zaporka');
                    l_obj.put('h_errcod', 102);
                    RAISE e_iznimka;
                WHEN OTHERS THEN
                    RAISE;
            END;

            SELECT
                    JSON_OBJECT
                ('ID' VALUE kor.id, 'ime' VALUE kor.ime, 'prezime' VALUE kor.prezime, 'email' VALUE kor.email, 'email' VALUE kor.email,
                                'password' VALUE kor.password, 'password' VALUE kor.password, 'ulica' VALUE kor.ulica, 'ulica' VALUE kor.
                                ulica)
            INTO l_record
            FROM
                korisnici kor
            WHERE
                id = l_id;

        END IF;

        l_out.append(json_object_t(l_record));
        l_obj.put('data', l_out);
        out_json := l_obj;
    EXCEPTION
        WHEN e_iznimka THEN
            out_json := l_obj;
        WHEN OTHERS THEN
            common.p_errlog('p_users_upd', dbms_utility.format_error_backtrace, sqlcode, sqlerrm, l_input);
            l_obj.put('h_message', 'Gre�ka u obradi podataka');
            l_obj.put('h_errcode', 99);
            ROLLBACK;
    END p_login;

END dohvat;
/
