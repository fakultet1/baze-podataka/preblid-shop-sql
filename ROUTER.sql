CREATE OR REPLACE PACKAGE ROUTER AS 
 e_iznimka exception;

 procedure p_main(p_in in varchar2, p_out out varchar2);
 
END ROUTER;
/


CREATE OR REPLACE PACKAGE BODY router AS

    PROCEDURE p_main (
        p_in  IN VARCHAR2,
        p_out OUT VARCHAR2
    ) AS
        l_obj       json_object_t;
        l_procedura VARCHAR2(50);
    BEGIN
        l_obj := json_object_t(p_in);
        SELECT
            JSON_VALUE(p_in, '$.procedura' RETURNING VARCHAR2)
        INTO l_procedura
        FROM
            dual;

        CASE l_procedura
            WHEN 'p_get_zaposlenici' THEN
                dohvat.p_get_zaposlenici(json_object_t(p_in), l_obj);
            WHEN 'p_get_korisnici' THEN
                dohvat.p_get_korisnici(json_object_t(p_in), l_obj);
            WHEN 'p_get_kategorije' THEN
                dohvat.p_get_kategorije(json_object_t(p_in), l_obj);
            WHEN 'p_get_proizvod' THEN
                dohvat.p_get_proizvod(json_object_t(p_in), l_obj);
            WHEN 'p_login' THEN
                dohvat.p_login(json_object_t(p_in), l_obj);
            WHEN 'p_spremi_kategoriju' THEN
                podaci.p_spremi_kategoriju(json_object_t(p_in), l_obj);
            WHEN 'p_spremi_korisnika' THEN
                podaci.p_spremi_korisnika(json_object_t(p_in), l_obj);
            WHEN 'p_spremi_proizvod' THEN
                podaci.p_spremi_proizvod(json_object_t(p_in), l_obj);
            WHEN 'p_spremi_zaposlenika' THEN
                podaci.p_spremi_zaposlenika(json_object_t(p_in), l_obj);
            ELSE
                l_obj.put('h_message', ' Nepoznata metoda ' || l_procedura);
                l_obj.put('h_errcode', 997);
        END CASE;

        p_out := l_obj.to_string;
    END p_main;

END router;
/
